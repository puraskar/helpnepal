<?php

namespace Help\Bundle\HelpNepalBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Help\Bundle\HelpNepalBundle\Entity\RequiredResource;
use Help\Bundle\HelpNepalBundle\Entity\AvailableResource;
use Help\Bundle\HelpNepalBundle\Form\Type\RequirementType;
use Help\Bundle\HelpNepalBundle\Form\Type\AvailableType;


class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        
        $professions = array(
            'choices' => array(
                'Dr'        => 'Volunteer Doctor', 
                'Nurse'     => 'Volunteer Nurse',
                'Volunteer' => 'Other Volunteer',
                'Medicine'  => 'Medicine',
                'Water'     => 'Water',
                'Dry_Food'  => 'Dry Food',
                'Tent'      => 'Tent',
                'Blanket'   => 'Blanket',
                'Cleaner'   => 'Cleaning Volunteer'
                )
            );
        
        $districts = array (
            'choices' => array(
                'Kathmandu'     => 'Kathmandu', 
                'Sindhupalchowk'=> 'Sindhupalchowk',
                'Nuwakot'       => 'Nuwakot',
                'Dhading'       => 'Dhading',
                'Gorkha'        => 'Gorkha',
                'Bhaktapur'     => 'Bhaktapur',
                'Rasuwa'        => 'Rasuwa'
                )
            );

        $requirement = new RequiredResource();
        $form_requirement = $this->createFormBuilder($requirement)
                ->add('profession', 'choice', $professions)
                ->add('address1', 'choice', $districts)
                ->add('address2', 'text', array('data' => 'n/a'))
                ->add('contact_phone', 'text')
                ->add('save_requirement', 'submit', array('label' => 'REQUEST HELP'))
                ->getForm();
        
        
        $availability = new AvailableResource();
        $form_availability = $this->createFormBuilder($availability)
                ->add('profession', 'choice', $professions)
                ->add('address1', 'choice', $districts)
                ->add('address2', 'text', array('data' => 'n/a'))
                ->add('contact_phone', 'text')
                ->add('contact_email', 'text', array('data' => 'n/a'))
                ->add('save_availability', 'submit', array('label' => 'OFFER HELP'))
                ->getForm();
        
        $em = $this->getDoctrine()->getManager();

        if ($this->getRequest()->isMethod('POST')) {
        
            $request = Request::createFromGlobals();
            
            $params = $request->request->all();
            
            if (isset($params['form']['save_requirement'])) {
                $form = $form_requirement;
            } else if (isset($params['form']['save_availability'])) {
                $form = $form_availability;
            }

            $form->handleRequest($request);
            $data = $form->getData();

            try {
                $em->persist($data);
                $em->flush();
                $response = new RedirectResponse('/');
                $response->prepare($request);
                return $response->send();
            } catch (\Exception $e) {
                
            }
        }
        
        $requiredList = $this->getDoctrine()
                ->getRepository('HelpHelpNepalBundle:RequiredResource')
                ->findAll();
        
        $availableList = $this->getDoctrine()
                ->getRepository('HelpHelpNepalBundle:AvailableResource')
                ->findAll();

        return  array(
            'form_requirement' => $form_requirement->createView(),
            'form_availability' => $form_availability->createView(),
            'requiredList' => $requiredList,
            'availableList' => $availableList
        );
    }
}
