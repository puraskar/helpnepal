<?php

namespace Help\Bundle\HelpNepalBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of RequiredType
 *
 * @author puraskar
 */
class RequirementType extends AbstractType
{
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('required_resource', 'select');
        $builder->add('required_resource_custom', 'text');
        $builder->add('district', 'select');
        $builder->add('village', 'text');
        $builder->add('contact_phone', 'text');
    }
    

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Help\Bundle\HelpNepalBundle\Entity\RequiredResource'
        ));
    }

    public function getName()
    {
        return 'requirement';
    }
}