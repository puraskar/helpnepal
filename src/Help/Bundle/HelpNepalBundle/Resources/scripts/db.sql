create table required_resource (id int(11) AUTO_INCREMENT Primary Key, 
profession varchar(100),
address_1 varchar(100),
address_2 varchar(100),
contact_phone varchar(100));

create table available_resource (id int(11) AUTO_INCREMENT Primary Key, 
profession varchar(100),
address_1 varchar(100),
address_2 varchar(100),
contact_phone varchar(100),
contact_email varchar(255));